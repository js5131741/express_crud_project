/* eslint-disable no-undef */

require('dotenv').config({ path: '.env' });

// Database connection
const DB_CONFIG = {
  username: process.env.DB_USERNAME || 'postgres',
  password: process.env.DB_PASSWORD || 'postgres',
  dialect: process.env.DB_DIALECT || 'postgres',
  database: process.env.DB_DATABASE || 'postgres',
  host: process.env.DB_HOST || 'localhost',
  port: process.env.DB_PORT || 5432,
};

const SERVER_PORT = process.env.SERVER_PORT || 3000;

module.exports = { DB_CONFIG, SERVER_PORT };
