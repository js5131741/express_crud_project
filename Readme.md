## Express CRUD project

1. Git clone the repository

`git clone https://gitlab.com/js5131741/express_crud_project.git`

2. Navigate to project directory

`cd express_crud`

3. Install all the dependencies

`npm install`

4. Ensure that your postgres connection is open. Then, create a table with the following query.

```
create table todos(
id serial primary key,
text varchar(255),
isCompleted boolean);
```

5. Once created the table, check the config.js file and create a .env file and enter the necessary details.

6. Run client.js

`node client.js`

7. Go to Postman and do CRUD operations.