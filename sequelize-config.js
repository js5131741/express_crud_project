/* eslint-disable no-undef */
const { Sequelize } = require('sequelize');
const { DB_CONFIG } = require('./config');
// Database connection
const sequelize = new Sequelize({
  username: DB_CONFIG.username,
  password: DB_CONFIG.password,
  dialect: DB_CONFIG.dialect,
  database: DB_CONFIG.database,
  host: DB_CONFIG.host,
  port: DB_CONFIG.port,
});

module.exports = sequelize;
