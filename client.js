/* eslint-disable no-undef */
const express = require('express');
const yup = require('yup');
const sequelize = require('./sequelize-config');
const { SERVER_PORT } = require('./config.js');

const app = express();
app.use(express.json());

const { DataTypes } = require('sequelize');

const Todo = sequelize.define(
  'Todo',
  {
    text: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    iscompleted: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
  },
  {
    tableName: 'todos',
    timestamps: false,
  },
);

const validateTodoSchema = yup.object().shape({
  text: yup.string().required(),
  iscompleted: yup.boolean().required(),
});

const testDbConnection = async () => {
  try {
    await sequelize.authenticate();
    console.log('Connection has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
};

testDbConnection();

const tryCatch = async (crudFunction, res) => {
  try {
    await crudFunction();
  } catch (error) {
    res.status(422).json({ message: 'Validation error', error: error.message });
  }
};

app.post('/todos', async (req, res) => {
  await tryCatch(async () => {
    const { text, iscompleted } = req.body;
    await validateTodoSchema.validate({ text, iscompleted });
    const todo = await Todo.create({ text, iscompleted });
    res.json(todo);
  }, res);
});

app.get('/todos', async (req, res) => {
  await tryCatch(async () => {
    const todos = await Todo.findAll();
    res.json({ todos });
  }, res);
});

app.get('/todos/:id', async (req, res) => {
  await tryCatch(async () => {
    const { id } = req.params;
    const todo = await Todo.findByPk(id);
    if (todo) {
      res.json(todo);
    } else {
      res.status(404).json({ message: 'Not found' });
    }
  }, res);
});

app.put('/todos/:id', async (req, res) => {
  await tryCatch(async () => {
    const { id } = req.params;
    const { text, iscompleted } = req.body;
    await validateTodoSchema.validate({ text, iscompleted });
    const todo = await Todo.findByPk(id);
    if (todo) {
      todo.text = text;
      todo.iscompleted = iscompleted;
      await todo.save();
      res.json(todo);
    } else {
      res.status(404).json({ message: 'Not found' });
    }
  }, res);
});

app.delete('/todos/:id', async (req, res) => {
  await tryCatch(async () => {
    const { id } = req.params;
    const todo = await Todo.findByPk(id);
    if (todo) {
      await todo.destroy();
      res.json({ message: 'Todo deleted successfully' });
    } else {
      res.status(404).json({ message: 'Not found' });
    }
  }, res);
});

app.listen(SERVER_PORT, () => {
  console.log(`Server is running on port ${SERVER_PORT}`);
});
